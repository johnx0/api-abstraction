
import java.io.*;
import java.util.*;

/**
 * A third-party API that we're using has a paginated API. It returns results in chunks.
 * This is implemented below on as FetchPageAPI.fetchPage()
 *
 * We don't think that API is very useful, and would like to implement a single function that
 * fetches n number of results from fetch page and abstracts away the pagination.
 *
 * Your task will be to implement Fetcher.fetch(), example: if you call for the first time Fetcher.fetch(35) then your method has to retrieve the first 35 results, your solution must use the FetchPageAPI.fetchPage() method to accomplish that (remember we want to abstract this method),the fetched results will be from 0 to 34. Now, if  you call the same method a second time say Fetcher.fetch(11) then you have to retrieve the next 11 elements starting from the 35th element, if you are asked to retrieve more elements than MAX_RESULTS (see FetchPageAPI) then return all the elements until MAX_RESULTS e.g Fetcher.fetch(145). Once you have returned all elements and are asked to return more elements then return an empty list.
 Please note that if you create a second instance of Fetcher it will start from element 0 so you can retrieve again the elements as needed (please refer to test case 6)
 * Please refer to the test cases when writing your solution.
 * Note: Please dont modify the FetchPageAPI class nor Result class.
 * Plus (optional): Give the asymptotic running time of your algorithm in Big-O notation
 */


class Result {

    private List<Integer> results;
    private int page;

    public Result(List<Integer> results, int page) {
        this.results = results;
        this.page = page;
    }

    public List<Integer> getResults() {
        return this.results;
    }

    public int getPage() {
        return this.page;
    }
}


class FetchPageAPI {

    private static final int MAX_RESULTS = 103;
    private static final int PAGE_SIZE = 10;

    private FetchPageAPI(){

        throw new IllegalStateException("Utility class");
    }


    public static Result fetchPage(int page) {
        /**
         * Return the next page of results. If page == 0, starts from the
         * beginning. Otherwise, fetches the next PAGE_SIZE records after the last page.
         * returns:
         *    {
         *       'results': [...],
         *       'nextPage': 3
         *    }
         */
        List<Integer> results = new ArrayList<>();

        if (page * PAGE_SIZE > MAX_RESULTS) {
            return new Result(results, -1);
        }
        results = makeRange(page * PAGE_SIZE, Math.min(MAX_RESULTS, (page + 1) * PAGE_SIZE));
        return new Result(results, page + 1);
    }

    public static List<Integer> makeRange(int min, int max) {
        List<Integer> results = new ArrayList<>();
        for (int i = min; i < max; i++) {
            results.add(i);
        }
        return results;
    }
}


class Fetcher {


    public List<Integer> fetch(int numResults) {

        // Start coding here, if you need more classes or methods please define them
        // inline.
        return new ArrayList<Integer>();

    }
}

class Solution {

    public static void main(String[] args) {
        Fetcher fetcher1 = new Fetcher();

        testCase(1, fetcher1.fetch(5), FetchPageAPI.makeRange(0, 5));
        testCase(2, fetcher1.fetch(2), FetchPageAPI.makeRange(5, 7));
        testCase(3, fetcher1.fetch(7), FetchPageAPI.makeRange(7, 14));
        testCase(4, fetcher1.fetch(103), FetchPageAPI.makeRange(14, 103));
        testCase(5, fetcher1.fetch(10), FetchPageAPI.makeRange(0, 0));
        Fetcher fetcher2 = new Fetcher();
        testCase(6, fetcher2.fetch(200), FetchPageAPI.makeRange(0, 103));

    }

    static void testCase(int caseNum, List<Integer> actual, List<Integer> expected) {
        if (actual.containsAll(expected) && expected.containsAll(actual)) {
            System.out.println("Test " + caseNum + " passed");
        } else {
            System.out.println("Test " + caseNum + " failed - expected: " + expected);
            System.out.println("actual: " + actual);
        }
    }
}







